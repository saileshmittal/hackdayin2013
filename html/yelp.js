var priority;
priority = {};

function handleClearCheckBox(cbS, id) {
    console.log(priority);
    if (cbS.checked) {
        console.log(cbS);
        var d = cbS.parentNode;
        var p = d.parentNode;
        p.parentNode.removeChild(p);
        console.log(priority);
        delete priority[id];
        console.log(priority);
    }
    calcRoute();
}

function handleCheckboxChange(cb, rating) {
    if (cb.checked && !(cb.value in priority)) {
        var list = document.getElementById("selectedDestinations");
        var p = document.createElement("p");
        list.appendChild(p);
        p.innerHTML = "<div id='"+cb.value+"'><input type='checkbox' onclick='handleClearCheckBox(this, \""+cb.value+"\");'>" + cb.value + "</div>";
        console.log(priority);
        priority[cb.value] = rating;
    }
    calcRoute();
}

function checkTimeLimit(time) {
    console.log(time);
    var endTime = document.getElementById('endtime').valueAsDate;
    var endHours = endTime.getHours() + 8; // PDT is -800 hrs
    var endMins = endTime.getMinutes();
    var endDate = document.getElementById("enddate").valueAsDate;
    endDate.setHours(endHours);
    endDate.setMinutes(endMins);
    console.log(endDate);

    var leastPlace;
    var leastRating = 6;
    for (k in priority) {
        // Also clear any background color.
        var e = document.getElementById(k);
        if (e) {
            e.style.background = "transparent";
        }
        if (leastRating > priority[k]) {
            leastPlace = k;
            leastRating = priority[k];
        }
    }

    if (endDate < time) {
        console.log("hellooooo");
        console.log(k);
        var ele = document.getElementById(leastPlace);
        if (ele) {
            ele.style.background = "red";
        }
    }
}

function yelpHelp(){
    var auth = {
        //
        // Update with your auth tokens.
        //

        consumerKey: "8ZZ5EyaVYRvFnvFPg2qpqQ",
        consumerSecret: "rRHiHiai4jIv1yiCdf24i8hN0WU",
        accessToken: "yi20fl2VF0HMNRHX0fWujYe8kY5rjplB",
        // This example is a proof of concept, for how to use the Yelp v2 API with javascript.
        // You wouldn't actually want to expose your access token secret like this in a real application.

        accessTokenSecret: "x8s0IMqVU2hl2G6JwmyHqvqqO6o",
        serviceProvider: {
            signatureMethod: "HMAC-SHA1"
        }
    };

    var terms = 'attractions';
    var near = document.getElementById('place1').value.replace(/ /g,'+');

    var accessor = {
        consumerSecret: auth.consumerSecret,
        tokenSecret: auth.accessTokenSecret
    };

    parameters = [];
    parameters.push(['term', terms]);
    parameters.push(['location', near]);
    parameters.push(['callback', 'cb']);
    parameters.push(['oauth_consumer_key', auth.consumerKey]);
    parameters.push(['oauth_consumer_secret', auth.consumerSecret]);
    parameters.push(['oauth_token', auth.accessToken]);
    parameters.push(['oauth_signature_method', 'HMAC-SHA1']);

    var message = {
        'action': 'http://api.yelp.com/v2/search',
        'method': 'GET',
        'parameters': parameters
    };

    OAuth.setTimestampAndNonce(message);
    OAuth.SignatureMethod.sign(message, accessor);

    var parameterMap = OAuth.getParameterMap(message.parameters);
    parameterMap.oauth_signature = OAuth.percentEncode(parameterMap.oauth_signature)
    console.log(parameterMap);


    $.ajax({
        'url': message.action,
        'data': parameterMap,
        'cache': true,
        'dataType': 'jsonp',
        'jsonpCallback': 'cb',
        'success': function(data, textStats, XMLHttpRequest) {
            console.log(data);
            var string = "";
            for (var b in data.businesses) {
                var business = data.businesses[b];
                var loc = business.location;
                var name = business.name;
                var link = '<a href=' + business.url + '>' + name + '</a>';
                var img = '<img src=' + business.rating_img_url + '> (of '+ business.review_count +' reviews)';
                string = string + "<div><input type='checkbox' id='" + "yelp"+b+"' value='" + name + ", " + loc.display_address.join(",") + "' onclick='handleCheckboxChange(this,"+business.rating+");'>" + link +"<br/>"+ img + "</div><hr/>";
            }
            var yelp = document.getElementById("yelp");
            yelp.innerHTML = string;
        }
    });
}