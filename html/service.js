var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var map;
var destinations;

function initialize() {
      destinations = {};
      // Date time init
      document.getElementById('startdate').valueAsDate = new Date();
      document.getElementById('starttime').valueAsDate = new Date();
      document.getElementById('enddate').valueAsDate = new Date();
      document.getElementById('endtime').valueAsDate = new Date();

      // Autocomplete Initializations start
      var input1 = /** @type {HTMLInputElement} */(document.getElementById('place1'));
      var autocomplete1 = new google.maps.places.Autocomplete(input1);

      google.maps.event.addListener(autocomplete1, 'place_changed', function() {
        input1.className = '';
        var place = autocomplete1.getPlace();
        if (!place.geometry) {
          // Inform the user that the place was not found and return.
          input1.className = 'notfound';
          return;
        }
        yelpHelp();
      });

        // Autocomplete Initializations start
      var input2 = /** @type {HTMLInputElement} */(document.getElementById('startloc'));
      var autocomplete2 = new google.maps.places.Autocomplete(input2);

      google.maps.event.addListener(autocomplete2, 'place_changed', function() {
        input2.className = '';
        var place = autocomplete2.getPlace();
        if (!place.geometry) {
          // Inform the user that the place was not found and return.
          input2.className = 'notfound';
          return;
        }
      });

        // Autocomplete Initializations start
      var input3 = /** @type {HTMLInputElement} */(document.getElementById('endloc'));
      var autocomplete3 = new google.maps.places.Autocomplete(input3);

      google.maps.event.addListener(autocomplete3, 'place_changed', function() {
        input3.className = '';
        var place = autocomplete3.getPlace();
        if (!place.geometry) {
          // Inform the user that the place was not found and return.
          input3.className = 'notfound';
          return;
        }
      });
    // Autocomplete Initializations end

    directionsDisplay = new google.maps.DirectionsRenderer();
    var chicago = new google.maps.LatLng(41.850033, -87.6500523);
    var mapOptions = {
        zoom:7,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: chicago
    }
    map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
    directionsDisplay.setMap(map);
}

function getWayPointsFromDestinations() {
    var list = document.getElementById("selectedDestinations");
    var ret = [];
    for (var i=0;i<list.children.length;i++) {
        ret.push({
            location: list.children[i].innerText,
            stopover:true
        });
    }
    return ret;
}

function updateTimeSpent(value, address) {
    destinations[address] = value;
    calcRoute();
}

function printClick() {
  var w = window.open();
  var html = $("#directions_panel").html();

  // how do I write the html to the new window with JQuery?
    $(w.document.body).html(html);
}

$(function() {
    $("a#print").click(printClick);
});

function calcRoute() {
    var start = document.getElementById("startloc").value;
    var end = document.getElementById("endloc").value;
    var request = {
        origin:start,
        destination:end,
        waypoints: getWayPointsFromDestinations(),
        optimizeWaypoints:true,
        travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService.route(request, function(result, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(result);
            var time = document.getElementById("starttime").valueAsDate;
            var startHours = time.getHours() + 8; // PDT is -800 hrs
            var startMins = time.getMinutes();
            var date = document.getElementById("startdate").valueAsDate;
            date.setHours(startHours);
            date.setMinutes(startMins);
            var startDate = date.getDate() + 1; // Due to PDT
            var startMonth = date.getMonth();
            var startYear = date.getYear();
            var movingDate = new Date(date);
            var route = result.routes[0];
            var summaryPanel = document.getElementById('directions_panel');
            summaryPanel.innerHTML = '';
            // For each route, display summary information.
            for (var i = 0; i < route.legs.length; i++) {
                var routeSegment = i + 1;
                summaryPanel.innerHTML += '<b>Route Segment: ' + routeSegment + '</b><br>';
                summaryPanel.innerHTML += route.legs[i].start_address + ' to ';
                summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                summaryPanel.innerHTML += route.legs[i].distance.text + '<br>';
                summaryPanel.innerHTML += route.legs[i].duration.text + '<br>';
                summaryPanel.innerHTML += 'Start time: ' + movingDate + '<br>';
                var bool1 = false;
                if(movingDate.getHours() < 12)
                {
                  bool1 = true;
                }
                movingDate.setSeconds(movingDate.getSeconds() + route.legs[i].duration.value);
                var bool2 = false;
                if(movingDate.getHours() >= 12)
                {
                  bool2 = true;
                }
                summaryPanel.innerHTML += 'Arrival time: ' + movingDate + '<br><br>';

                if(bool1 && bool2)
                {
                  summaryPanel.innerHTML += '<b>Time for Food !!! Cry for <a href="http://www.yelp.com/">Yelp!</a></b><br/><br/>'
                }

                if (i < route.legs.length-1) {
                    var id = "route"+i;
                    var timeSpent = destinations[route.legs[i].end_address] || 2;
                    var address = route.legs[i].end_address;
                    var inputTextBox = 'Time you wish to spend here: <input type=textbox id="' + id + '" value='+timeSpent+' onChange="updateTimeSpent(this.value,\''+address+'\');">hrs';
                    summaryPanel.innerHTML += inputTextBox;
                    movingDate.setSeconds(movingDate.getSeconds() + timeSpent*3600);
                } else {
                    checkTimeLimit(movingDate);
                }
                summaryPanel.innerHTML += '<br><br>';
            }
        }
    });
}
