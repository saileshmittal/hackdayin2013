#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""tourlinkin.py: Main module of the app which takes input from user."""

__author__ = "Vishwas B Sharma, Sailesh Mittal"
__email__ = "sharma.vishwas88@gmail.com, mittalsailesh@gmail.com"

import webapp2
import logging
from os import path

class MainPage(webapp2.RequestHandler):
  def get(self):
      page = 'homepage.html'
      home = open(page, 'rb').read()
      self.response.out.write(home)


app = webapp2.WSGIApplication([('/', MainPage)],
                              debug=True)
